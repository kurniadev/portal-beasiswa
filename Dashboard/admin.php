<!DOCTYPE html>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="{!! asset('asset/css/styledashboard.css') !!}">
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="styles.css" />
    <title>Dashboard</title>
  </head>
  <body id="body">
    <div class="container">
      <nav class="navbar">
        <div class="nav_icon" onclick="toggleSidebar()">
          <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
        <div class="navbar__left">
          <a class="active_link" href="#">Admin</a>
        </div>
        <div class="navbar__right">
          <a href="#">
            <i class="fa fa-search" aria-hidden="true"></i>
          </a>
          <a href="#">
            <i class="fa fa-clock-o" aria-hidden="true"></i>
          </a>
          <a href="#">
            <img width="30" src="{!! asset('asset/img/avatar.png') !!}" alt="" />
            <!-- <i class="fa fa-user-circle-o" aria-hidden="true"></i> -->
          </a>
        </div>
      </nav>

      <main>
        <div class="main__container">
          <!-- MAIN TITLE STARTS HERE -->

          <div class="main__title">
            <img src="{!! asset('asset/img/hello.png') !!}" alt="" />
            <div class="main__greeting">
              <h1>Beasiswaku  </h1>
              <p>Selamat Datang di Dashboard</p>
            </div>
          </div>

          <!-- MAIN TITLE ENDS HERE -->

          <!-- MAIN CARDS STARTS HERE -->
          <div class="main__cards">
            <div class="card">
              <i
                class="fa fa-user-o fa-2x text-lightblue"
                aria-hidden="true"
              ></i>
              <div class="card_inner">
                <p class="text-primary-p">Data Pendaftar</p>
                <span class="font-bold text-title">578</span>
              </div>
            </div>

            <div class="card">
              <i class="fa fa-user-o fa-2x text-lightblue" aria-hidden="true"></i>
              <div class="card_inner">
                <p class="text-primary-p">Data Pendaftar</p>
                <span class="font-bold text-title">2467</span>
              </div>
            </div>

            <div class="card">
              <i
                class="fa fa-user-o fa-2x text-lightblue"
                aria-hidden="true"
              ></i>
              <div class="card_inner">
                <p class="text-primary-p">Data Beasiswa</p>
                <span class="font-bold text-title">340</span>
              </div>
            </div>

            <div class="card">
              <i
                class="fa fa-book fa-2x text-lightblue"
                aria-hidden="true"
              ></i>
              <div class="card_inner">
                <p class="text-primary-p">Kritik <br> & Saran </p>
                <span class="font-bold text-title"> 645</span>
              </div>
            </div>
          </div>
          <!-- MAIN CARDS ENDS HERE -->

          <!-- CHARTS STARTS HERE -->
          <div class="charts">
            <div class="charts__left">
              <div class="charts__left__title">
                <div>
                  <h1>Data Mahasiswa</h1>
                </div>
                <i class="fa fa-user-o fa-2x text-lightblue" aria-hidden="true"></i>
              </div>
            </div>

            <div class="charts__right">
              <div class="charts__right__title">
                <div>
                  <h1>Jenis Beasiswa</h1>
                </div>
                <i class="fa fa-user-o fa-2x text-lightblue" aria-hidden="true"></i>
              </div>

              <div class="charts__right__cards">
                <div class="card1">
                  <h1>Contoh 1</h1>
                  <p>Uniersitas Negeri Surabaya</p>
                </div>

                <div class="card2">
                  <h1>Contoh 2</h1>
                  <p>Uniersitas Negeri Surabaya</p>
                </div>

                <div class="card3">
                  <h1>Contoh 3</h1>
                  <p>Uniersitas Negeri Surabaya</p>
                </div>

                <div class="card4">
                  <h1>Contoh 4</h1>
                  <p>Uniersitas Negeri Surabaya</p>
                </div>
              </div>
            </div>
          </div>
          <!-- CHARTS ENDS HERE -->
        </div>
      </main>

      <div id="sidebar">
        <div class="sidebar__title">
          <div class="sidebar__img">
            <img src="{!! asset('asset/img/logo-dashboard.png') !!}" alt="logo" />
            <h1>Beasiswaku</h1>
          </div>
          <i
            onclick="closeSidebar()"
            class="fa fa-times"
            id="sidebarIcon"
            aria-hidden="true"
          ></i>
        </div>
        <div class="my-profil">
              <div class="card_inner1">
                <p class="text-myprofile">My Profile</p>
              </div>
              <img src="{!! asset('asset/img/user.png') !!}" >
              <div class="card_inner2">
                <p class="text-myprofile">Reza Kurnia Setiawan</p>
              </div>
        </div>

        <div class="sidebar__menu">
          <div class="sidebar__link active_menu_link">
            <i class="fa fa-home"></i>
            <a href="#">Dashboard</a>
          </div>
          <div class="sidebar__link">
          <i class="fa fa-user-o fa-2x " aria-hidden="true"></i>
            <a href="#">Data Pendaftar</a>
          </div>
          <div class="sidebar__link">
          <i class="fa fa-user-o fa-2x " aria-hidden="true"></i>
            <a href="#">Data Mahasiswa</a>
          </div>
          <div class="sidebar__link">
          <i class="fa fa-user-o fa-2x " aria-hidden="true"></i>
            <a href="#">Data Beasiswa</a>
          </div>
          <div class="sidebar__link">
            <i class="fa fa-archive"></i>
            <a href="#">Kritik & Saran</a>
          </div>
          <div class="sidebar__logout">
            <i class="fa fa-power-off"></i>
            <a href="#">Log out</a>
          </div>
        </div>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="{!! asset('asset/js/script.js') !!}"></script>
  </body>
</html>
