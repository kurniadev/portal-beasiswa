<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('asset/css/bootstrap.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('asset/css/style.css') !!}">
    <title>Beasiswaku</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

</head>

<body>
    <div class="garis-navbar-atas"></div>
    <nav>
        <div class="logo" >
            <h3>BEASISWAKU</h3>
        </div>

        <ul>
            <li><a href="">Beranda</a></li>
            <li><a href="">Panduan Daftar</a></li>
            <li><a href="">Tentang Kami</a></li>
            <li><a href="{{url('/login')}}" class="button1" style="vertical-align:middle"><i style="font-size:15px" class="fa">&#xf090;</i><span> LOGIN </span></a></li>
        </ul>
        <div class="menu-toggle" >
            <input type="checkbox" ></input>
            <span></span>
            <span></span>
            <span></span>
        </div>
  </nav>
    <div class="garis-navbar-bawah"></div>
    
    <div class="footer">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-4">
                <center>
                <div class="posisi-footer"><img src="{!! asset('asset/img/logo1.png') !!}" alt="logo"></div>
                <div class="deskripsi-footer">
                    <p>Beasiswa menurut KBBI yaitu beasiswa merupakan tunjangan uang yang diberikan kepada pelajar atau mahasiswa sebagai bantuan biaya belajar.</p>
                </div>
                </center>
            </div>
            <div class="col-lg-3">
                <center>
                    <div class="posisi-footer"><b>NAVIGASI</b></div>
                    <center>
                        <div class="garis"></div>
                    </center>
                    <div class="navigasi">
                        <a href="" class="navigasi1">Panduan Daftar</a><br>
                        <a href="" class="navigasi1">Hubungi Kami</a>
                    </div>
                </center>
            </div>
            <div class="col-lg-3">
                <center>
                <div class="posisi-footer"><b>HUBUNGI KAMI</b></div>
                <div class="garis"></div>
                <div class="deskripsi-footer">
                    <i style="font-size:32px" class="fa">&#xf041;</i>
                    Pulorejo , Tembelang, Jombang<br>
                    <i style="font-size:24px" class="fa">&#xf0e0;</i>
                    join.beasiswaku@gmail.com
                </div>
                </center>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
    <div class="copyright">
        <center> © 2021 | Beasiswaku </center>
    </div>
</body>
<script src="{!! asset('asset/js/nav.js') !!}"></script>

</html>