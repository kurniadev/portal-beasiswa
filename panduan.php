<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('asset/css/style.css') !!}">

    <title>Panduan Daftar</title>
</head>

<body>
    <div class="garis-navbar-atas"></div>
    <div class="Navbar">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-3">
                <div class="logo"><img src="{!! asset('asset/img/logo.png') !!}" alt="logo"></div>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-6">
                <div class="topnav">
                    <a href="/"><b>BERANDA</b></a>
                    <a href="/panduan"><b>PANDUAN DAFTAR</b></a>
                    <a href="#"><b>TENTANG KAMI</b></a>
                    <form method="get" action="/login">
                        <button href="/login" class="button1" style="vertical-align:middle"><i style="font-size:15px" class="fa">&#xf090;</i><span>LOGIN </span></button>
                    </form>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
    <div class="garis-navbar-bawah"></div>
    <div class="all">
        <div class="bg-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="panduan">
                            <b>PANDUAN DAFTAR</b>
                        </div>
                        <center>
                            <div class="garis"></div>
                        </center>
                        <div class="fontaja">
                            <p style="color: red"><b>LANGKAH 1</b></p>
                            <p style=" margin-left: 30px;"><b>Daftarkan dirimu</b></p>
                            <div class="penjelasan">
                                <p>Syarat daftar:</p>
                                <p> 1. Mahasiswa/i aktif UNESA</p>
                                <p> 2. Menginputkan bukti berupa KTM maupun KRS</p>
                            </div>
                            <div style="text-align: right;">
                                <p style="color: red; "><b>LANGKAH 2</b></p>
                                <p style="margin-left: 30px;"><b>Tunggu konfirmasi admin ya!</b></p>
                                <div class="penjelasan">
                                    <p>Konfirmasi akan dikirim 2x24 jam melalui alamat E-mail.</p>
                                    <p>Hubungi admin jika tidak mendapatkan notifikasi</p>
                                </div>
                            </div>
                            <p style="color: red; "><b>LANGKAH 3</b></p>
                            <p style="margin-left: 30px;"><b>Yeay, selamat!</b></p>
                            <div class="penjelasan">
                                <p>Sobat beasiswa dapat login dan mengetahui informasi beasiswa terupdate</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <img src="{!! asset('asset/img/vector1.png') !!}" width="508px" height="528px">
                    </div>
                    <div class="garis-deskripsi"></div>
                </div>
            </div>
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
            <div class="footer">
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-4">
                        <div class="posisi-footer"><img src="{!! asset('asset/img/logo1.png') !!}" alt="logo"></div>
                        <div class="deskripsi-footer">
                            <p>Beasiswa menurut KBBI yaitu beasiswa merupakan tunjangan uang yang diberikan kepada pelajar atau mahasiswa sebagai bantuan biaya belajar.</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <center>
                            <div class="posisi-footer"><b>NAVIGASI</b></div>
                            <center>
                                <div class="garis"></div>
                            </center>
                            <div class="navigasi">
                                <a href="" class="navigasi1">Panduan Daftar</a><br>
                                <a href="" class="navigasi1">Hubungi Kami</a>
                            </div>
                        </center>
                    </div>
                    <div class="col-lg-3">
                        <div class="posisi-footer"><b>HUBUNGI KAMI</b></div>
                        <div class="garis"></div>
                        <div class="deskripsi-footer">
                            <i style="font-size:32px" class="fa">&#xf041;</i>
                            Pulorejo , Tembelang, Jombang<br>
                            <i style="font-size:24px" class="fa">&#xf0e0;</i>
                            join.beasiswaku@gmail.com
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
            <div class="copyright">
                <center> © 2021 | Beasiswaku </center>
            </div>
</body>
</div>

</html>